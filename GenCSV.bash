#!/bin/bash
###############################################################################
#
#    GenCSV.bash
#
#    Gera o CSV de todos os PDFs na directoria que é passada como primeiro parâmetro.
#    Os restantes parâmetros indicam as colunas do csv - todas as colunas se não houver
#    mais nenhum parâmetro.
#    
#    Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# config

# no more config

###############################################################################
# start bash

if [  $# -lt 1 ]; then 
    echo Utilização: $0 "<directoria com PDFs a processar> [lista de coluna(s)]"
    echo Nota: se lista de colunas for vazia, o output incluirá todas as colunas
    echo
    echo Exemplo: $0 ISO
    echo Exemplo: $0 ISO WG \"DOC NUMBER\" TITLE DATE STATUS ACTION \"DUE DATE\" SOURCE \"NO. OF PAGES\" \> part.csv
    exit
fi

directoria=$1; shift;

# verifica se existe directoria
if [ ! -d $directoria ]; then 
    echo Directoria $directoria inexistente ...
    exit
fi

# verifica se existem PDFs na directoria
files=`ls $directoria/*.pdf 2> /dev/null`;
if [ $? != 0 ]; then
    echo Não foram encontrados ficheiros PDF na directoria $directoria  ...
    exit
fi

# processa documentos
perl ISO2csv.pl -l "$@"
for f in $directoria/*.pdf; do
#    echo "$f"
#    echo "pdftotext -f 1 -l 1 -raw $f - | perl ISO2csv.pl - $@"
    pdftotext -f 1 -l 1 -raw "$f" - | perl ISO2csv.pl - "$@"
done


