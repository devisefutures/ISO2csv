# ISO2csv

Projecto no âmbito do CT163 de modo a facilitar a elaboração do indíce (em csv e html) dos documentos recebidos. 


## Utilização

#### 1. Faça o clone deste repositório (ou download da última versão)

``` bash
git clone git@gitlab.com:devisefutures/ISO2csv.git
cd ISO2csv
```

#### 2. Configure a secção Configuração da Makefile e da MakeGenCSV.bash

Note que ISO_DIR é a directoria onde se encontram as remessas em formato zip que quer processar, pelo que poderá ter
que efectuar previamente o mount (davfs) da mesma.

#### 3. Coloque as sub-directorias css, fonts e js (incluidas na directoria csv-to-html-table) como
sub-directoria de ISO_DIR (variável configurada no passo anterior).

#### 4. Execute a Makefile

A Makefile permite gerar os csv e html de todos os documentos PDFs e DOCs nos zips 
(o MakeGenCSV.bash é feito à medida para ser utilizado pela Makefile). 

Para gerar o csv e html de todos os zips:
1. Efectue duas vezes 'make unzip'
2. Efectue 'make csv'
3. Efectue 'make html' - note que em cada directoria é criado um ficheiro index.html

#### 5. Aceda ao URL para visualizar o indice

O URL é o BASE_HREF/index.html (BASE_HREF foi configurado no passo 3, na Makefile).

Se não conseguir aceder, veja se o URL está correcto, se tem as permissões necessárias e se a configuração do servidor Web está a apontar 
para o local correcto.

#### 6. Apagar os ficheiros e directorias criadas (opcional)

* Para limpar as directorias criadas, efectue 'make dirclean'
* Para limpar os csv's criadas, efectue 'make csvclean'
* Para limpar os html's criadas, efectue 'make csvclean'
* Para limpar tudo o que foi criado pela Makefile, efectue 'make clean'

## Utilização alternativa

Em alternativa, pode utilizar o ISO2csv.pl para cada ficheiro PDF ou o GenCSV.bash para processar todos os PDFs numa directoria, 
do seguinte modo:

    GenCSV.bash <directoria com PDFs a processar> [lista de coluna(s)]
    
    Nota: se lista de colunas for vazia, o output incluirá todas as colunas. Para além das colunas indicadas, poderá ser inserida 
    NomeColuna[Valor] que adicionará uma coluna NomeColuna sempre com o conteúdo Valor (a utilizar por exemplo para adicionar número 
    da remessa) 

    Exemplo: ./GenCSV.bash ISO
    Exemplo: ./GenCSV.bash ISO WG "DOC NUMBER" TITLE DATE STATUS ACTION "DUE DATE" SOURCE "NO. OF PAGES" > output.csv
    Exemplo: ./GenCSV.bash ISO REMESSA[ISO123] WG "DOC NUMBER" TITLE DATE STATUS ACTION "DUE DATE" SOURCE "NO. OF PAGES" > output.csv
    
Foi ainda disponibilizado uma script cujo objectivo é simplificar o unzip de vários ficheiros. Deve ser utilizada do
seguinte modo:

    unZIP.bash <ficheiro(s) a aplicar unzip> <parâmetros a passar a unzip>
    
    Exemplo: ./unZIP.bash 12*.zip

## Dependências

Para utilizar necessita de ambiente Unix/Linux com as seguintes aplicações:
  + perl 5 - testado com versão 18.2
  + pdftotext - testado com versão 0.26.5
  + bash - testado com versão 4.3.30
  + make - testado com GNU Make 4.0
  + abiword - testado com abiword 3.0.0

Para a apresentação do csv em html é utilizado o csv-to-html-table (https://github.com/derekeder/csv-to-html-table), tendo as
directorias css, fonts e js do commit 3971db7 de 22/Fev/2016 sido copiadas para a directoria csv-to-html-table. 

## Erros / bugs / Ideias 

Agradecemos que nos reporte todos os erros, bugs ou ideias de novas funcionalidades.
Para tal, utilize https://gitlab.com/devisefutures/ISO2csv/issues

## Desenvolvimento de novas funcionalidades

Se pretende efectuar desenvolvimento de novas funcionalidades:
* Faça um fork do projecto,
* Efectue as alterações pretendidas,
* Envie um pedido de merge.

## Copyright

Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -. Distribuído sob [Licença GNU GPL] (https://gitlab.com/devisefutures/ISO2csv/blob/master/LICENSE).
