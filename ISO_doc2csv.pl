#!/usr/bin/perl -w
###############################################################################
#
#    ISO_doc2csv.pl
#
#    Transforma um documento Word da ISO (previamente passado por
#    abiword) numa linha de csv, em que as colunas são os headers escolhidos
#    
#    Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

#pragmas
use strict;
use warnings;

#core Perl modules
use IO::Handle;

# Variavel Global (our)
our ($VERSION, $VERBOSE, $PARAMS, $COMMA) = ("1.00", 0, 1, ";");
our @COLS = ("DOC NUMBER", "TITLE", "DATE", "PROJECT", "NO. OF PAGES"); 
our @OLCOLS = (); # colunas só com uma linha no Word, de modo a não adicionar outras linhas
our %OTHERCOLS = (); # hash utilizada para as colunas NomeColuna[Valor], em que $OTHERCOLS{NomeColuna} = Valor

my @args = GetArg ($PARAMS, "Pega no output de 'abiword --to=txt --to-name=fd://1 <Word ISO>' e gera o correspondente csv com as colunas pretendidas (" . join(", ", @COLS) . ") para o STDOUT. " .
                        "Para além das colunas indicadas, poderá ser inserida NomeColuna[Valor] que adicionará uma coluna NomeColuna sempre com o conteúdo Valor (a utilizar por exemplo para adicionar número da remessa). " .
                        "Por questão de compatibilidade com o ISO2csv.pl, aceita também nome de colunas desconhecidas, caso em que não colocará qualquer valor nas mesmas. " .
                        "Se as colunas forem vazias, gera csv com todas as colunas referidas anteriormente.\nDe notar que infile pode ser '-' (stdin)." .
                        "\n\t -l - apenas são geradas para o STDOUT o csv com o nome das colunas a processar", "[-l] infile [coluna(s)]");


warn "> Analizando lista de colunas pretendidas ...\n" if $VERBOSE;

my @selected_cols = ();
if ($#args >= $PARAMS) {
    @selected_cols = Verify_Cols(@args[$PARAMS..$#args]);
} else {
    @selected_cols = @COLS;
}

if ($args[0] eq "-l") {
    Print_csv(@selected_cols);
} else {
    Create_CSV($args[0], $VERBOSE, @selected_cols);
}

exit 0;

###############################################################################
###############################################################################

#_______________________________________________________________________________
# GetArg : Valida a linha de comando
# Argumentos:
#   $param - Número de parametros (para além do -v utilizado para warnings e verbosing, e do -h utilizado para help);
#   $info - Texto de info sobre programa;
#   $usage - Restantes parâmetros;
# Pré-condições: Declaração de variavel Global 'our $VERBOSE = 0'

sub GetArg {
    my ($param, $info, $usage)= @_;

    if ($#ARGV >= 0 && $ARGV[0] eq "-v") {
        $VERBOSE = 1;
        shift @ARGV;
    }
    
    warn "> Analizando comando linha ...\n" if $VERBOSE;

    if (($#ARGV >= 0 && $ARGV[0] eq "-h") || ($#ARGV < $param - 1)) {
        print "Utilizção: $0 [-v] [-h] $usage\n";
        print "$info\n\t -h - ajuda\n\t -v - verbose\n";
        exit;
    }
    
    return @ARGV;
}
#______________________________________________________________________________



#_______________________________________________________________________________
# Create_CSV: Gera o CSV para o STDOUT a partir do ficheiro $in
# Argumentos:
#   $in - ficheiro de input; stdin se "-"
#   $verb - verbose, booleano
#   @sel_col - lista de colunas seleccionadas a serem escritas no csv
#
# Pré-condições: -
#
# Valor de retorno: 0, se não houver erro

sub Create_CSV {
    my ($in, $verb, @sel_col)= @_;
    my $txt = ""; # vai conter texto a escrever para o csv
    my $col = -1; # vai conter indice da coluna da sel_col a processar
    my @csv = map "", 1..$#sel_col; # vai conter dados a escrever no csv
    my $ln = -1; # contar as linhas para Project
    my $infile;

    if ($in eq "-") {
        $infile = *STDIN;
    } else {
        open $infile, "$in"   or die "Impossivel abrir ficheiro $in : $!";
    }

    warn "> A analisar ficheiro de input $infile ...\n" if $verb;
 
    while (<$infile>) {
        if ((/^.+ comments on .+$/) && (($col = Lindex("TITLE", @sel_col)) >= 0)) { # TITLE escolhido para stdout
            $csv[$col] = $_;
        } elsif ((/^Date: (.+)$/) && (($col = Lindex("DATE", @sel_col)) >= 0)) { # DATE escolhido para stdout
            $csv[$col] = $1;
        } elsif ((/^Document: (.+)$/) && (($col = Lindex("DOC NUMBER", @sel_col)) >= 0)) { # DOC NUMBER escolhido para stdout
            $csv[$col] = $1;
        } elsif ((/^page (\d+) of (\d+)$/) && (($col = Lindex("NO. OF PAGES", @sel_col)) >= 0)) { # NO. OF PAGES escolhido para stdout
            $csv[$col] = $2;
        } elsif ((/^Project:(.*)$/) && (($col = Lindex("PROJECT", @sel_col)) >= 0)) { # PROJECT escolhido para stdout
            $txt = $1;
            $ln = 0;
        } elsif ($ln >=0 && $ln < 7){
            $txt .= " $_";
            if (++$ln == 6) {
                $csv[$col] = $txt;
            }
        } else {
            $ln = $col = -1;
            $txt = "";
        }
    }
    
    close $infile;
    
    # Adiciona valor constante a coluna NomeColuna[Valor]
    foreach my $k (keys %OTHERCOLS) {
        $csv[Lindex($k, @sel_col)] = $OTHERCOLS{$k};
    }
    
    warn "> A gerar csv para STDOUT...\n" if $verb;
    Print_csv(@csv);  
    
    return 0;
}
#______________________________________________________________________________




#_______________________________________________________________________________
# Verify_Cols: Verifica se a lista de colunas passadas como argumento estão contidas em @COLS ou se é do tipo especial NomeColuna[Valor]
#       Se a coluna não for conhecida, é tratado com tipo especial NomeColuna[Valor], sendo o valor a string vazia.
# Argumentos:
#   @lc - lista de colunas
#
# Pré-condições: -
#
# Valor de retorno: @lc, se não houver erro

sub Verify_Cols {
    my @lc = @_;

    foreach my $c (@lc) {
        if ( Lindex($c, @COLS) < 0) { # se a coluna $c não pertence a @COLS
            if ($c =~ /(.+)\[(.+)\]/) { # valida se está no formato NomeColuna[Valor]
                $lc[Lindex($c, @lc)] = $1;
                $OTHERCOLS{$1} = $2;
            } else {
                $OTHERCOLS{$c} = "";
            }
        }
    }

    return @lc;
}

#______________________________________________________________________________



#_______________________________________________________________________________
# Lindex: Devolve o indice de $e na lista @l
# Argumentos:
#   $e - elemento
#   @l - lista 
#
# Pré-condições: -
#
# Valor de retorno: indice de $e na lista @l, ou -1 se $e não pertencer a @l

sub Lindex {
    my ($e, @l) = @_;
    my $idx = -1;

    if ($e eq "ACTION ID") {$e = "ACTION";} # Estas duas colunas são a mesma coisa e referidas por nome diferente consoante o documento

    foreach my $c (@l) {
        $idx++;
        if ( (uc $e) eq (uc $c) ) {
            return $idx;
        }
    }
       
    return -1;
}

#______________________________________________________________________________



#_______________________________________________________________________________
# Add_str2str: Adiciona $s1 a $s2 (separando com ',') se $s1 ainda não estiver incluido em $s2
# Argumentos:
#   $s1 - string a adicionar a $s2
#   $s2 - string 
#
# Pré-condições: -
#
# Valor de retorno: string

sub Add_str2str {
    my ($s1, $s2) = @_;

    if (index ($s2, $s1) == -1) { # se $s2 não contiver $s1
        if ($s2 eq "") {return $s1;}
        else {return "$s2, $s1";}       
    }

    return $s2;
}

#______________________________________________________________________________



#_______________________________________________________________________________
# Print_csv: Imprime lista @l em formato csv para STDOUT
# Argumentos:
#   @l - lista 
#
# Pré-condições: -
#
# Valor de retorno: 0

sub Print_csv {
    my @l = @_;

    foreach my $c (@l)
    {
        $c =~ tr/\n\cL/  /; # substitui new lines com espaços
        $c =~ tr/"/'/; # substitui " com single quote
#        if (index($c, $COMMA) != -1) { # se a string tiver $COMMA
#            print STDOUT '"' . $c . '"' . $COMMA;
#        } else {
#            print STDOUT $c . $COMMA;
#        }
        print STDOUT '"' . $c . '"' . $COMMA; # engloba tudo o que é para escrever com "
    }
    print STDOUT "\n";
    return 0;
}

#______________________________________________________________________________






1;