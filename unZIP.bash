#!/bin/bash
###############################################################################
#
#    unZIP.bash
#
#    Expande o argumento para fazer o unzip de todos os ficheiros
#    
#    Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# config

# no more config

###############################################################################
# start bash

if [  $# -lt 1 ]; then 
    echo Utilização: $0 "<ficheiro(s) a aplicar unzip> <parâmetros a passar a unzip>"
    echo
    echo Exemplo: $0 "14*.zip"
    exit
fi

zips=$1; shift;


# verifica se existem os zips
files=`ls $zips 2> /dev/null`;
if [ $? != 0 ]; then
    echo Não foram encontrados ficheiros $zips  ...
    exit
fi

# efectua unzip
for f in $zips; do
    echo "unzip $f $@"
    unzip "$f" "$@"
done


