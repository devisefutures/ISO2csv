#!/bin/bash
###############################################################################
#
#    MakeGenCSV.bash
#
#    Gerador de CSV, invocado pela Makefile (feito à medida para ser utilizado 
#    pela Makefile). 
#    Gera o CSV de todos os documentos na directoria que é passada como primeiro parâmetro,
#    adicionando os .<subdirectoria>.csv das subdirectorias.
#    Os restantes parâmetros indicam as colunas do csv - todas as colunas se não houver
#    mais nenhum parâmetro.
#    Nota: Não gera o header do CSV para o stdout
#    Nota: Se uma das colunas for FILENAME, gerará o nome do ficheiro para essa coluna
#    
#    Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Configuração

# Localização dos vários executáveis necessários
PDFTOTEXT=/usr/bin/pdftotext
PERL=/usr/bin/perl
ABIWORD=/usr/bin/abiword

#Localização das scripts 
ISO2CSV=/home/jepm/ISO2csv/ISO2csv.pl
ISO_DOC2CSV=/home/jepm/ISO2csv/ISO_doc2csv.pl

# Localização dos top-level zips da ISO
ISO_DIR=./ISO
#ISO_DIR ?= /media/CT163/JTC1-SC27.index

# end Configuração

###############################################################################
# start bash

if [  $# -lt 1 ]; then 
    echo Utilização: $0 -l|-r "<directoria com documentos a processar> [lista de coluna(s)]"
    echo Nota: se lista de colunas for vazia, o output incluirá todas as colunas
    echo Nota: -r Listagem inversa dos ficheiros da directoria, no csv
    echo Nota: -l Listagem normal dos ficheiros da directoria, no csv
    echo
    echo Exemplo: $0 ISO -r
    echo Exemplo: $0 ISO -l WG \"DOC NUMBER\" TITLE DATE STATUS ACTION \"DUE DATE\" SOURCE \"NO. OF PAGES\" \> part.csv
    exit
fi

directoria=$1; shift;

# verifica se existe directoria
if [ ! -d $directoria ]; then 
    echo Directoria $directoria inexistente ...
    exit
fi

g=$directoria/*

formato=$1; shift;

if [ "${formato,,}" == "-r" ]; then
    # Processa directoria de modo à listagem ser inversa
    g=`echo $g | sed 's/ /\n/g' | sort -r`
fi

# processa documentos na directoria passada como parâmetro
for f in $g; do
    filename=$(basename "$f")
    ext="${filename##*.}"
    if [ -d "$f" ]; then # se for directoria
        dotcsvfile=$f/_`basename $f`.csv;
        if [ -e "$dotcsvfile" ]; then # se existir .<subdirectoria>.csv
            cat "$dotcsvfile"
        fi
    elif [[ $(head -c 4 "$f") == "%PDF" ]]; then # se for PDF
        # verifica se a lista de colunas contém FILENAME
        p=();
        for e in "$@"; do
            if [ "${e^^}" == "FILENAME" ]; then
                bname=`basename "$f"`;
                d="$ISO_DIR/";
                b="${f/$d/}";
                p+=("FILENAME[<a href='$b' target='_blank'>$bname</a>]");
            else
                p+=("$e");
            fi
        done
        $PDFTOTEXT -f 1 -l 1 -raw "$f" - | $PERL $ISO2CSV - "${p[@]}"
    elif [ "${ext,,}" == "doc" ]; then # se for Word (extensão doc)
        # verifica se a lista de colunas contém FILENAME
        p=();
        for e in "$@"; do
            if [ "${e^^}" == "FILENAME" ]; then
                bname=`basename "$f"`;
                d="$ISO_DIR/";
                b="${f/$d/}";
                p+=("FILENAME[<a href='$b'>$bname</a>]");
            else
                p+=("$e");
            fi
        done
        $ABIWORD --to=txt --to-name=fd://1 "$f" | $PERL $ISO_DOC2CSV - "${p[@]}"
    fi
done

