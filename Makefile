###############################################################################
#
#    Makefile
#
#    Automatiza toda a geração de csv e html partindo das remessas zip do ISO
#
#    Nota: Deve ser passado o argumento ISO_DIR com a path para a directoria dos zips
#          Se o ISO_DIR não for passado como parâmetro, parte-se do principio que o make
#	   se aplica à directoria configurada na secção de Configuração na variavel ISO_DIR
#
#    Exemplo: make ISO_DIR="./ISO"
#    Exemplo: make
#
#    Quando se aplica a Makefile pela primeira vez, o
#	+ primeiro make faz o unzip do primeiro nível de zips
#	+ segundo make faz o unzip dos zips nas subdirectorias criadas no make anterior
#
#
#    Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


##############################################################################
# Configuração - estes parâmetros podem ser passados pela linha de comando
#
# Localização dos top-level zips da ISO
#ISO_DIR ?= /media/CT163/JTC1-SC27.index
ISO_DIR ?= ./ISO
#
# Zip's a processar
ZIP_PROC ?= 14[4-9]*.zip
# Directorias criadas
DIR_CREATE ?= 14[4-9]*
#
# Ficheiro csv final com todos os documentos, disponível na ISO_DIR
TOTAL_CSV ?= total.csv
# Ficheiro html a criar por directoria com o indice
#HTML_INDEX ?= index.html
HTML_INDEX ?= index_body.html
# Base href - URL base para a directoria ISO_DIR
#BASE_HREF ?= https:\/\/www.paranoidjasmine.com\/CT163\/
BASE_HREF ?= https:\/\/ct163.paranoidjasmine.com\/dashboard\/CT163\/
#
# Localização do construtor do index.html
#HTML_CONS ?= /home/jepm/ISO2csv/index.html
HTML_CONS ?= /home/jepm/ISO2csv/index_body.html
# Localização da script ISO2csv.pl
ISO2CSV ?= /home/jepm/ISO2csv/ISO2csv.pl
#
# Colunas a serem colocadas no csv - Nota: sempre que for alterado, fazer um make csvclean, seguido de make
COLUNAS ?= WG "DOC NUMBER" TITLE DATE STATUS ACTION "DUE DATE" SOURCE PROJECT
#
# Localização do MakeGenCSV.bash
MAKEGENCSV ?= /home/jepm/ISO2csv/MakeGenCSV.bash
# Localização dos vários executáveis necessários
PERL ?= /usr/bin/perl
#
# Aplicação de mount
MOUNT ?= /bin/mount
# Aplicação de umount
UMOUNT ?= /bin/umount
# Mount Point
MOUNT_DIR ?= /media/jepm@CT163
# Localização remota dos zips, no mount
MOUNT_ZIPDIR ?= /media/jepm@CT163/JTC1-SC27
#
# Fim de Configuração
##############################################################################

###########
# Variáveis
###########
ZIPFILES = $(wildcard $(ISO_DIR)/$(ZIP_PROC))
SUBZIPFILES = $(wildcard $(ISO_DIR)/$(DIR_CREATE)/*.zip)
CSVFILES = $(wildcard $(ISO_DIR)/*.csv) $(wildcard $(ISO_DIR)/$(DIR_CREATE)/*.csv) $(wildcard $(ISO_DIR)/$(DIR_CREATE)/*/*.csv) $(wildcard $(ISO_DIR)/_*.csv) $(wildcard $(ISO_DIR)/$(DIR_CREATE)/_*.csv) $(wildcard $(ISO_DIR)/$(DIR_CREATE)/*/_*.csv)
HTMLFILES = $(wildcard $(ISO_DIR)/$(HTML_INDEX)) $(wildcard $(ISO_DIR)/$(DIR_CREATE)/$(HTML_INDEX)) $(wildcard $(ISO_DIR)/$(DIR_CREATE)/*/$(HTML_INDEX))
SUBDIRS = $(foreach e, $(wildcard $(ISO_DIR)/$(DIR_CREATE)), $(shell if [ -d $(e) ]; then echo $(e); fi))
SUBSUBDIRS = $(foreach e, $(wildcard $(ISO_DIR)/$(DIR_CREATE)/*), $(shell if [ -d $(e) ]; then echo $(e); fi))
REMOTE_ZIPFILES = $(foreach e, $(wildcard $(MOUNT_ZIPDIR)/$(ZIP_PROC)), `basename $(e) .zip`)


####################
# Funções e outros
####################

# devolve lisa com a diferença de duas listas
diff_list =  $(shell printf "%s\n" $(1) $(2) | sort | uniq -u | tr '\n' ' ')

# copia zips ($1) de ($2) para $(3). A cada elemento de $(1) há que adicionar a extensão .zip
copy_list_from_to = $(foreach e, $(1), cp $(2)/$(e).zip $(3);)


# devolve o basename de todos os elementos da lista passada como primeiro argumento
base_name = $(foreach e, $(1), $(shell basename $(e)))

# devolve a path para todos os csv a construir nas subdirectorias da directoria passada como primeiro argumento
subdir_csv = $(foreach e, $(wildcard $(1)/*), $(shell if [ -d $(e) ]; then echo $(e)/`basename $(e)`.csv; fi))

define extract_zip
$(shell dirname $(1))/$(shell basename $(1) .zip): $(1)
	unzip -u $(1) -d `dirname $(1)`/`basename $(1) .zip`
	find `dirname $(1)`/`basename $(1) .zip` -depth -name "*[ \,\;\?\'\"\!\[\]\(\)\@\#\&]*" -execdir rename "s/[ \,\;\?\'\"\!\[\]\(\)\@\#\&]/_/g" "{}" \;
endef

define csv_files
$(1)/$(2).csv: $(1)/_$(2).csv
	$(PERL) $(ISO2CSV) -l "REMESSA[<a href='$(3)/index.html'>$(3)</a>]" $(COLUNAS) FILENAME[-] > $(1)/$(2).csv
	cat $(1)/_$(2).csv >> $(1)/$(2).csv
endef

define dotcsv_files
$(1)/_$(2).csv: $(3)
	$(MAKEGENCSV) $(1) -l "REMESSA[<a href='$(4)/index.html'>$(4)</a>]" $(COLUNAS) FILENAME > $(1)/_$(2).csv
endef

define html_files
$(2): $(3)/$(1).csv
	cp $(HTML_CONS) $(2)
	sed -i 's/%BASE_HREF%/<base href="$(BASE_HREF)">/' $(2)
	sed -i 's/%REMESSA%/ - Remessa $(1)/' $(2)
	sed -i "s/%INDICE_REM%/$(INDICE_REMESSAS)/" $(2)
	sed -i 's/%CSV_PATH%/csv_path: "$(1)\/$(1).csv",/' $(2)
endef

ZIP_DIRS := $(foreach file, $(ZIPFILES), $(shell dirname $(file))/$(shell basename $(file) .zip))
#ZIP_DIRS := $(foreach file, $(ZIPFILES), $file)
SUBZIP_DIRS := $(foreach file, $(SUBZIPFILES), $(shell dirname $(file))/$(shell basename $(file) .zip))
#SUBZIP_DIRS := $(foreach file, $(SUBZIPFILES), $file)

DIR_CSV := $(foreach d, $(SUBDIRS), $(d)/$(shell basename $(d)).csv)
TOP_CSV := $(ISO_DIR)/$(TOTAL_CSV)

DIR_HTML := $(foreach d, $(SUBDIRS), $(d)/$(HTML_INDEX))
TOP_HTML := $(ISO_DIR)/$(HTML_INDEX)

INDICE_REMESSAS := $(foreach d, $(shell echo $(SUBDIRS) | sed 's/ /\n/g' | sort -r), <a href='$(shell basename $(d))\/$(HTML_INDEX)'>$(shell basename $(d))<\/a>)

##########
# Regras
##########


default:
	@ echo
	@ echo "Para gerar o csv e html de todos os zips:"
	@ echo "\t 1 - Configure a Makefile (secção Configuração)"
	@ echo "\t 2 - Efectue duas vezes 'make unzip'"
	@ echo "\t 3 - Efectue 'make csv'"
	@ echo "\t 4 - Efectue 'make html'"
	@ echo
	@ echo "Para limpar as directorias criadas, efectue 'make dirclean'"
	@ echo "Para limpar os csv's criadas, efectue 'make csvclean'"
	@ echo "Para limpar os html's criadas, efectue 'make htmlclean'"
	@ echo "Para limpar os zip's, efectue 'make zipclean'"
	@ echo "Para limpar os csv's e html's criados pela Makefile, efectue 'make clean'"
	@ echo
	@ echo "Nota: O make não aceita ficheiros/directorias com (, ), & e espaços"
	@ echo

#all: mount remotecopy umount unzip unzip zipclean csv htmlclean html

mount:
	$(MOUNT) $(MOUNT_DIR)

umount:
	$(UMOUNT) $(MOUNT_DIR)

remotecopy:
	$(call copy_list_from_to,$(call diff_list,$(REMOTE_ZIPFILES),$(call base_name,$(SUBDIRS))),$(MOUNT_ZIPDIR),$(ISO_DIR))


unzip: $(ZIP_DIRS) $(SUBZIP_DIRS)
	@ echo
	@ echo "NOTA: Para fazer o unzip de todos os ficheiros terá que efectuar duas vezez 'make unzip'"
	@ echo

csv: $(TOP_CSV)

$(TOP_CSV): $(DIR_CSV)
	$(PERL) $(ISO2CSV) -l REMESSA[-] $(COLUNAS) FILENAME[-] > $(TOP_CSV)
	$(MAKEGENCSV) $(ISO_DIR) -r REMESSA[-] $(COLUNAS) FILENAME >> $(TOP_CSV)

html: $(TOP_HTML)

$(TOP_HTML): $(DIR_HTML)
	cp $(HTML_CONS) $(TOP_HTML)
	sed -i 's/%BASE_HREF%/<base href="$(BASE_HREF)">/' $(TOP_HTML)
	sed -i "s/%INDICE_REM%/$(INDICE_REMESSAS)/" $(TOP_HTML)
	sed -i 's/%REMESSA%//' $(TOP_HTML)
	sed -i 's/%CSV_PATH%/csv_path: "$(TOTAL_CSV)",/' $(TOP_HTML)


# remove as directorias
dirclean:
	rm -rf $(ZIP_DIRS)

# remove os zip's
zipclean:
	rm -f $(ZIPFILES) $(SUBZIPFILES)

# remove os csv's
csvclean:
	rm -f $(CSVFILES)

# remove os html's
htmlclean:
	rm -f $(HTMLFILES)

clean: csvclean htmlclean

$(foreach file, $(ZIPFILES), $(eval $(call extract_zip, $(file))))
$(foreach file, $(SUBZIPFILES), $(eval $(call extract_zip, $(file))))

$(foreach d, $(SUBSUBDIRS), $(eval $(call csv_files,$(d),$(shell basename $(d)),$(shell basename $(shell dirname $(d))))))
$(foreach d, $(SUBSUBDIRS), $(eval $(call dotcsv_files,$(d),$(shell basename $(d)),,$(shell basename $(shell dirname $(d))))))

$(foreach d, $(SUBDIRS), $(eval $(call csv_files,$(d),$(shell basename $(d)),$(shell basename $(d)))))
$(foreach d, $(SUBDIRS), $(eval $(call dotcsv_files,$(d),$(shell basename $(d)),$(call subdir_csv,$(d)),$(shell basename $(d)))))

$(foreach d, $(SUBDIRS), $(eval $(call html_files,$(shell basename $(d)),$(d)/$(HTML_INDEX),$(d))))


.PHONY: default unzip csv html mount umount
.PHONY: dirclean csvclean clean htmlclean zipclean
