#!/usr/bin/perl -w
###############################################################################
#
#    ISO2csv.pl
#
#    Transforma a primeira página de um documento PDF da ISO (previamente passado por
#    pdftotext) numa linha de csv, em que as colunas são os headers escolhidos
#    
#    Copyright (C) 2016 Devise Futures, Lda - www.devisefutures.com -
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

#pragmas
use strict;
use warnings;

#core Perl modules
use IO::Handle;

# Variavel Global (our)
our ($VERSION, $VERBOSE, $PARAMS, $COMMA) = ("2.10", 0, 1, ";");
our @COLS = ("DOC NUMBER", "REPLACES", "Secretariat", "DOC TYPE", "TITLE", "SOURCE", "DATE", "PROJECT",
                "STATUS", "ACTION", "DUE DATE", "DISTRIBUTION", "MEDIUM", "NO. OF PAGES", "WG"); # Nota: "ACTION ID" vai reverter para ACTION
our @OLCOLS = ("REPLACES", "DATE","PROJECT"); # colunas só com uma linha no PDF, de modo a não adicionar outras linhas
our %OTHERCOLS = (); # hash utilizada para as colunas NomeColuna[Valor], em que $OTHERCOLS{NomeColuna} = Valor

my @args = GetArg ($PARAMS, "Pega no output de 'pdftotext -f 1 -l 1 -raw <PDF ISO> -' e gera o correspondente csv com as colunas pretendidas (" . join(", ", @COLS) . ") para o STDOUT. " .
                        "Para além das colunas indicadas, poderá ser inserida NomeColuna[Valor] que adicionará uma coluna NomeColuna sempre com o conteúdo Valor (a utilizar por exemplo para adicionar número da remessa). " .
                        "Se as colunas forem vazias, gera csv com todas as colunas indicadas.\nDe notar que infile pode ser '-' (stdin)." .
                        "\n\t -l - apenas são geradas para o STDOUT o csv com o nome das colunas a processar", "[-l] infile [coluna(s)]");


warn "> Analizando lista de colunas pretendidas ...\n" if $VERBOSE;

my @selected_cols = ();
if ($#args >= $PARAMS) {
    @selected_cols = Verify_Cols(@args[$PARAMS..$#args]);
} else {
    @selected_cols = @COLS;
}

if ($args[0] eq "-l") {
    Print_csv(@selected_cols);
} else {
    Create_CSV($args[0], $VERBOSE, @selected_cols);
}

exit 0;

###############################################################################
###############################################################################

#_______________________________________________________________________________
# GetArg : Valida a linha de comando
# Argumentos:
#   $param - Número de parametros (para além do -v utilizado para warnings e verbosing, e do -h utilizado para help);
#   $info - Texto de info sobre programa;
#   $usage - Restantes parâmetros;
# Pré-condições: Declaração de variavel Global 'our $VERBOSE = 0'

sub GetArg {
    my ($param, $info, $usage)= @_;

    if ($#ARGV >= 0 && $ARGV[0] eq "-v") {
        $VERBOSE = 1;
        shift @ARGV;
    }
    
    warn "> Analizando comando linha ...\n" if $VERBOSE;

    if (($#ARGV >= 0 && $ARGV[0] eq "-h") || ($#ARGV < $param - 1)) {
        print "Utilizção: $0 [-v] [-h] $usage\n";
        print "$info\n\t -h - ajuda\n\t -v - verbose\n";
        exit;
    }
    
    return @ARGV;
}
#______________________________________________________________________________



#_______________________________________________________________________________
# Create_CSV: Gera o CSV para o STDOUT a partir do ficheiro $in
# Argumentos:
#   $in - ficheiro de input; stdin se "-"
#   $verb - verbose, booleano
#   @sel_col - lista de colunas seleccionadas a serem escritas no csv
#
# Pré-condições: -
#
# Valor de retorno: 0, se não houver erro

sub Create_CSV {
    my ($in, $verb, @sel_col)= @_;
    my $txt = ""; # vai conter texto a escrever para o csv
    my $col = -1; # vai conter indice da coluna da sel_col a processar
    my @csv = map "", 1..$#sel_col; # vai conter dados a escrever no csv
    my $wg = Lindex("WG", @sel_col); # verifica se a coluna WG foi pedida para output
    my $infile;

    if ($in eq "-") {
        $infile = *STDIN;
    } else {
        open $infile, "$in"   or die "Impossivel abrir ficheiro $in : $!";
    }

    warn "> A analisar ficheiro de input $infile ...\n" if $verb;
 
    while (<$infile>) {
        if ($wg != -1) { # analisar a linha para ver se tem referência a id de WG, já que WG é output
            my $t = $_;
            while ($t =~ /.*?WG\s+(\d+)/g) { 
                if ($1 > 0 && $1 < 6) { # despista contra apontadores para WG de outros grupos ISO
                    $csv[$wg] = Add_str2str("WG $1", $csv[$wg]);
                }
            }
        }
        if (/^(.+?):\s*$/) { # analisar a linha para referência às outras colunas
            if ( Lindex($1, @COLS) >= 0 ) { # se é uma das colunas do ISO
                $csv[$col] = $txt if $col >= 0;
                $col = Lindex($1, @sel_col);
                $txt = "";
                $csv[$col] = "" if $col >= 0;
                $col = -1;
            } else {
                $txt .= " $_";
            }
        } elsif (/^(.+?):\s*(.+)$/) {
            if ( Lindex($1, @COLS) >= 0 ) { # se é uma das colunas do ISO
                $csv[$col] = $txt if $col >= 0;
                $col = Lindex($1, @sel_col);
                $txt = $2;
                if ( Lindex($1, @OLCOLS) >= 0 ) { # se esta coluna só tem uma linha no PDF
                    if (! (($1 eq "PROJECT") && (substr ($2, -1) eq ";")) ) { # se não (PROJECT e não acabar em ";") 
                        $csv[$col] = $txt if $col >= 0;
                        $col = -1;
                        $txt = "";
                    }
                }
            } else {
                $txt .= " $_";
            }
        } elsif (/^(.+)(N\d+)$/) { # especifico para obter DOC NUMBER
            my $cDN = Lindex("DOC NUMBER", @sel_col);
            if ($cDN >= 0 && $csv[$cDN] eq "") {
                $csv[$cDN] = $2;
                $col = -1;
                $txt = "";
            } else {
                $txt .= " $_";
            }
        } else {
            $txt .= " $_";
        }
    }
    $csv[$col] = $txt if $col >= 0;
    
    close $infile;
    
    # Adiciona valor constante a coluna NomeColuna[Valor]
    foreach my $k (keys %OTHERCOLS) {
        $csv[Lindex($k, @sel_col)] = $OTHERCOLS{$k};
    }
    
    warn "> A gerar csv para STDOUT...\n" if $verb;
    Print_csv(@csv);  
    
    return 0;
}
#______________________________________________________________________________




#_______________________________________________________________________________
# Verify_Cols: Verifica se a lista de colunas passadas como argumento estão contidas em @COLS ou se é do tipo especial NomeColuna[Valor]
# Argumentos:
#   @lc - lista de colunas
#
# Pré-condições: -
#
# Valor de retorno: @lc, se não houver erro

sub Verify_Cols {
    my @lc = @_;
    my $err = 0;

    foreach my $c (@lc) {
        if ( Lindex($c, @COLS) < 0) { # se a coluna $c não pertence a @COLS
            if ($c =~ /(.+)\[(.+)\]/) { # valida se está no formato NomeColuna[Valor]
                $lc[Lindex($c, @lc)] = $1;
                $OTHERCOLS{$1} = $2;
            } else {
                warn "Erro: $c não é nome de coluna conhecida. Por favor verifique argumentos da linha de comando.\n";
                $err++;
            }
        }
    }

    die "Impossivel continuar processamento da linha de comando. \n" if $err;
    
    return @lc;
}

#______________________________________________________________________________



#_______________________________________________________________________________
# Lindex: Devolve o indice de $e na lista @l
# Argumentos:
#   $e - elemento
#   @l - lista 
#
# Pré-condições: -
#
# Valor de retorno: indice de $e na lista @l, ou -1 se $e não pertencer a @l

sub Lindex {
    my ($e, @l) = @_;
    my $idx = -1;

    if ($e eq "ACTION ID") {$e = "ACTION";} # Estas duas colunas são a mesma coisa e referidas por nome diferente consoante o documento

    foreach my $c (@l) {
        $idx++;
        if ( (uc $e) eq (uc $c) ) {
            return $idx;
        }
    }
       
    return -1;
}

#______________________________________________________________________________



#_______________________________________________________________________________
# Add_str2str: Adiciona $s1 a $s2 (separando com ',') se $s1 ainda não estiver incluido em $s2
# Argumentos:
#   $s1 - string a adicionar a $s2
#   $s2 - string 
#
# Pré-condições: -
#
# Valor de retorno: string

sub Add_str2str {
    my ($s1, $s2) = @_;

    if (index ($s2, $s1) == -1) { # se $s2 não contiver $s1
        if ($s2 eq "") {return $s1;}
        else {return "$s2, $s1";}       
    }

    return $s2;
}

#______________________________________________________________________________



#_______________________________________________________________________________
# Print_csv: Imprime lista @l em formato csv para STDOUT
# Argumentos:
#   @l - lista 
#
# Pré-condições: -
#
# Valor de retorno: 0

sub Print_csv {
    my @l = @_;

    foreach my $c (@l)
    {
        $c =~ tr/\n\cL/  /; # substitui new lines com espaços
        $c =~ tr/"/'/; # substitui " com single quote
#        if (index($c, $COMMA) != -1) { # se a string tiver $COMMA
#            print STDOUT '"' . $c . '"' . $COMMA;
#        } else {
#            print STDOUT $c . $COMMA;
#        }
        print STDOUT '"' . $c . '"' . $COMMA; # engloba tudo o que é para escrever com "
    }
    print STDOUT "\n";
    return 0;
}

#______________________________________________________________________________






1;